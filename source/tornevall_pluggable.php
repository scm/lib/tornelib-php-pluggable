<?php

/**
 * TorneLIB plugin loader
 * @package TorneLIB
 */

namespace TorneLIB;

/**
 * Libraries and plugins that are internally handled by TorneLIB
 *
 * Class TorneLIB_Pluggable
 * @package TorneLIB
 * @version 6.0.0
 */
class TorneLIB_Pluggable
{

    /** @var array Plugin list */
    private $pluggableList = array();

    /**
     * TorneLIB_Pluggable constructor.
     */
    function __construct()
    {
    	$this->initPluggables();
    }

	/**
	 * Prepare list of plugins
	 */
    private function initPluggables() {
    	$classList = get_declared_classes();
    	foreach ($classList as $class) {
    		if (preg_match("/^tornelib/i", $class)) {
    			if (preg_match("/_plugin_/i", $class)) {
    				$plugClass = strtolower(preg_replace("/^(.*?)_plugin_(.*?)$/i", '$2', $class));
    				if (!isset($this->pluggableList[$plugClass])) {
					    $this->pluggableList[ $plugClass ] = $class;
				    }
			    }
		    }
	    }
    }

	/**
	 * Get list of loadable plugins
	 * @return array
	 */
    public function getPlugins() {
	    return $this->pluggableList;
    }

    /**
     * Initialize pluggable applications (plugins)
     * @param string $pluggableName
     * @return mixed
     * @since 5.0.0
     */
    public function initPluggable($pluggableName = '')
    {
    	return $this->initPlugin($pluggableName);
    }

	/**
	 * @param string $pluginName
	 *
	 * @return mixed
	 * @throws \Exception
	 * @since 5.0.0
	 */
	public function initPlugin( $pluginName = '' ) {

		if ( !empty($pluginName) && isset( $this->pluggableList[ strtolower( $pluginName ) ] ) ) {
			$plugClass          = $this->pluggableList[ strtolower( $pluginName ) ];
			$newPluggableLoader = new $plugClass;
			return $newPluggableLoader;
		} else {
			throw new \Exception("Exception " . __CLASS__ . "\\" . __FUNCTION__ . ": No such plugin available", 404);
		}
	}
}
