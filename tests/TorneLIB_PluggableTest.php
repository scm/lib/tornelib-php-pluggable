<?php

use TorneLIB\TorneLIB_Pluggable;
use PHPUnit\Framework\TestCase;

require_once '../vendor/autoload.php';

class TorneLIB_PluggableTest extends TestCase {

	/** @var TorneLIB_Pluggable */
	private $PLUG;
	/** @var \TorneLIB\TorneLIB_Plugin_Smarty */
	private $SMARTY;

	function setUp() {
		$this->PLUG = new TorneLIB_Pluggable();
	}

	function testPlug() {
		$this->assertTrue(is_object($this->PLUG));
	}

	function testGetPlugs() {
		$pluginList = $this->PLUG->getPlugins();
		// Assert the default plugin is there
		$this->assertCount(1, $pluginList);
	}
	function testInitSmarty() {
		try {
			$this->SMARTY = $this->PLUG->initPluggable( "smarty" );
			//$content      = json_decode( $this->SMARTY->template( "template1.txt" ) );
			//$this->assertStringStartsWith( "Test test test", $content->string );
		} catch (\Exception $initException) {
			$this->assertTrue($initException->getCode() > 0);
		}
	}
}
